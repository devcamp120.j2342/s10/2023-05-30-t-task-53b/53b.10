import model.Author;
import model.Book;

public class App {
    public static void main(String[] args) throws Exception {
        Author author1 = new Author("author1", "author1@devcamp", 'm');
        Author author2 = new Author("author2", "author2@devcamp", 'f');

        System.out.println(author1.toString());
        System.out.println(author2.toString());

        Book book1 = new Book("book1", author1, 1000);
        Book book2 = new Book("book2", author2, 5000, 10);

        System.out.println(book1.toString());
        System.out.println(book2.toString());
    }
}
